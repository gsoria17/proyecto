'use strict';

module.exports = (sequelize, DataTypes) => {
  const Token = sequelize.define('Token', {
    token: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    usuarios_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    nombre_usuario: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    // saldo_usuario: {
    //   type: DataTypes.INTEGER,
    //   allowNull: false,
    // }
    
  }, { tableName: 'tokens'});
  
  return Token;
};
