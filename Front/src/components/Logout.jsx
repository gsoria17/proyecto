import React, { Component } from 'react';

import { Redirect } from 'react-router-dom';
import {API} from './Config';

class Logout extends Component {
    constructor() {
      super();
      //Set default message
      this.state = {
        username: '',
        password: '',
        tornar: false
      }
      this.logout = this.logout.bind(this);
    }

    logout() {
      let token =  this.props.cookies.get('token');
      fetch(API+'/usuarios/logout', {
        method: 'DELETE', 
        headers: new Headers({ 'Content-Type': 'application/json' }),
        body: JSON.stringify({token})
      })
      .then(res => {
        console.log(res.json());
        //cal eliminar les cookies!
        this.props.cookies.remove("nombre");
        this.props.cookies.remove("token");
        this.props.cookies.remove("id");
        this.setState({tornar: true})
      })
      .catch(err => console.log(err));
    }

  

    render() {

      
      if (this.state.tornar === true) {
        return <Redirect to='/' />
      }

      let token =  this.props.cookies.get('token');
      if (!token) {
        return <h2>Usuario no conectado</h2>
      }


      return (
        <>
          <h1>Logout</h1>
            <button onClick={this.logout} className="button logout">Logout</button>
          
        </>
      );
    }
  }

  export default Logout;