import React, { Component } from 'react';
import {API} from './Config';

export default class Recarga extends Component {
    constructor(props) {
      super(props);
      this.state = {
        message: "no hay money",

      }
      this.recarga=this.recarga.bind(this);
    }

    recarga() {
      
      let token =  this.props.cookies.get('token');
      let importe = 10;
      if (token){
        fetch(API+'/usuarios/recargar', {
          method: 'PUT', 
          headers: new Headers({ 'Content-Type': 'application/json' }),
          body: JSON.stringify({token,importe})
        })
        .then(resp => resp.json())
        .then(resp => {
          console.log(resp)
          if(resp.ok){
            this.setState({message: resp.dinero});

            
          }
        })
      }

      
      
    }

    render() {
        
      return (
        <div>
          <h1>Saldo {this.state.message}</h1>
        
          <button onClick= {this.recarga} type="button" class="btn btn-success">Recargar</button>
          
        </div>
      );
    }
  }