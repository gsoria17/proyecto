import React, { Component } from 'react';
import {withCookies, Cookies } from 'react-cookie';
import { Redirect } from 'react-router-dom';
import {API} from './Config';

class Login extends Component {
    constructor() {
      super();
      //Set default message
      this.state = {
        nombre: '',
        password: '',
        tornar: false
      }
      this.submit = this.submit.bind(this);
      this.canvia = this.canvia.bind(this);
    }

    canvia(event) {
      const v =  event.target.value;
      const n = event.target.name;
      this.setState({
          [n]: v
      });
   }

    submit(e){
     
      e.preventDefault();
      let nombre= this.state.nombre;
      let password = this.state.password;
      let data = { nombre, password };
      
      fetch(API+'/usuarios/login', {
        method: 'POST', 
        headers: new Headers({ 'Content-Type': 'application/json' }),
        body: JSON.stringify(data)
      })
      .then(respuesta => respuesta.json())
      .then(respuesta => {
        if (respuesta.ok===false){
          throw respuesta.error;
        } else {
          return respuesta.data;
        }
      })
      .then(token => {
        console.log(token);
        if(token){
          console.log("establint cookies");
          this.props.cookies.set('nombre', token.nombre_usuario, {path: '/'});
          this.props.cookies.set('id', token.usuarios_id, {path: '/'});
          this.props.cookies.set('token', token.token, {path: '/'});
          this.setState({tornar:true});
        }
      })
      .catch(err => console.log(err));
    }

  

    render() {

      if (this.state.tornar === true) {
          return <Redirect to='/' />
      }

      return (
        <>
          <h1>Login</h1>
          <form className="login-form" onSubmit={this.submit}>
            <input onChange={this.canvia} type="text" name="nombre" value={this.state.nombre} placeholder="Nombre" />
            <br />
            <input onChange={this.canvia} type="text" name="password"  value={this.state.password} placeholder="Password" />
            <br />
            <button type="submit" className="button login">Login</button>
          </form>
         
        </>
      );
    }
  }

  export default withCookies(Login);