import React from 'react';
import { NavLink } from "react-router-dom";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';
  import imglogo from "../Imagenes/log.png";
export default class NavMenu extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div>
        <Navbar color="primary" light expand="md">
          <NavbarBrand href="/"><span>  <img src={imglogo}  alt="logo"/>  </span></NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink className="link nav-link" to="/">Home</NavLink>
              </NavItem>
              <NavItem>
              <NavLink className="link nav-link" to="/Login">Acceder</NavLink>
              </NavItem>
              <NavItem>
              <NavLink className="link nav-link" to="/Logout">Salir</NavLink>
              </NavItem>
           
              <UncontrolledDropdown nav inNavbar>


              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}