import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { API } from './Config';

export default class Home extends Component {
  constructor() {
    super();

    this.state = {
      nombre: '',

      password: '',
      tornar: false
    }
    this.submit = this.submit.bind(this);
    this.canvia = this.canvia.bind(this);
  }

  canvia(event) {
    const v = event.target.value;
    const n = event.target.name;
    this.setState({
      [n]: v
    });
  }

  submit(e) {
    e.preventDefault();
    let nombre = this.state.nombre;
    let password = this.state.password;
    let data = { nombre, password };

    fetch(API + '/usuarios/registre', {
      method: 'POST',
      headers: new Headers({ 'Content-Type': 'application/json' }),
      body: JSON.stringify(data)
    })
      .then(res => res.json())
      .then(res => {
        console.log(res);

        if (res.ok === true) {
          return fetch(API + '/usuarios/login', {
            method: 'POST',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify(data)
          })
        }
        else {
          throw "usuario no registrado";
        }

      })
      .then(res => res.json())
      .then(res => {
        const token = res.data;
        console.log(token);
        if (token) {
          console.log("establint cookies");
          this.props.cookies.set('nombre', token.nombre_usuario, { path: '/' });
          this.props.cookies.set('id', token.usuarios_id, { path: '/' });
          this.props.cookies.set('token', token.token, { path: '/' });
          this.setState({ tornar: true });
        }
      })
      .catch(err => console.log(err));
  }


  render() {

    if (this.state.tornar === true) {
      return <Redirect to='/' />
    }


    return (
      <>
        <h1>Registro</h1>
        <form onSubmit={this.submit} className="signup-form">
          <input onChange={this.canvia} type="text" name="nombre" value={this.state.nombre} placeholder="Nombre" />
          <br />


          <input onChange={this.canvia} type="text" name="password" value={this.state.password} placeholder="Password" />
          <br />
          <button type="submit" className="signup">Registro</button>
        </form>

      </>
    );
  }
}